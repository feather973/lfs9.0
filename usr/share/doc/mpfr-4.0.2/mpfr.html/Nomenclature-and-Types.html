<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents how to install and use the Multiple Precision
Floating-Point Reliable Library, version 4.0.2.

Copyright 1991, 1993-2019 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU Free Documentation License, Version 1.2 or any later
version published by the Free Software Foundation; with no Invariant Sections,
with no Front-Cover Texts, and with no Back-Cover Texts.  A copy of the
license is included in GNU Free Documentation License. -->
<!-- Created by GNU Texinfo 6.6, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Nomenclature and Types (GNU MPFR 4.0.2)</title>

<meta name="description" content="How to install and use GNU MPFR, a library for reliable multiple precision
floating-point arithmetic, version 4.0.2.">
<meta name="keywords" content="Nomenclature and Types (GNU MPFR 4.0.2)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Concept-Index.html#Concept-Index" rel="index" title="Concept Index">
<link href="MPFR-Basics.html#MPFR-Basics" rel="up" title="MPFR Basics">
<link href="MPFR-Variable-Conventions.html#MPFR-Variable-Conventions" rel="next" title="MPFR Variable Conventions">
<link href="Headers-and-Libraries.html#Headers-and-Libraries" rel="prev" title="Headers and Libraries">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Nomenclature-and-Types"></span><div class="header">
<p>
Next: <a href="MPFR-Variable-Conventions.html#MPFR-Variable-Conventions" accesskey="n" rel="next">MPFR Variable Conventions</a>, Previous: <a href="Headers-and-Libraries.html#Headers-and-Libraries" accesskey="p" rel="prev">Headers and Libraries</a>, Up: <a href="MPFR-Basics.html#MPFR-Basics" accesskey="u" rel="up">MPFR Basics</a> &nbsp; [<a href="Concept-Index.html#Concept-Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Nomenclature-and-Types-1"></span><h3 class="section">4.2 Nomenclature and Types</h3>

<span id="index-Floating_002dpoint-number"></span>
<span id="index-Regular-number"></span>
<span id="index-mpfr_005ft"></span>
<span id="index-mpfr_005fptr"></span>
<p>A <em>floating-point number</em>, or <em>float</em> for short, is an object
representing a radix-2 floating-point number consisting of a sign,
an arbitrary-precision normalized significand (also called mantissa),
and an exponent (an integer in some given range); these are called
<em>regular numbers</em>. Like in the IEEE 754 standard, a floating-point
number can also have three kinds of special values: a signed zero, a
signed infinity, and Not-a-Number (NaN). NaN can represent the default
value of a floating-point object and the result of some operations for
which no other results would make sense, such as 0 divided by 0 or
+Infinity minus +Infinity; unless documented otherwise, the sign bit of
a NaN is unspecified. Note that contrary to IEEE 754, MPFR has a single
kind of NaN and does not have subnormals. Other than that, the behavior
is very similar to IEEE 754, but there may be some differences.
</p>
<p>The C data type for such objects is <code>mpfr_t</code>, internally defined
as a one-element array of a structure (so that when passed as an
argument to a function, it is the pointer that is actually passed),
and <code>mpfr_ptr</code> is the C data type representing a pointer to this
structure.
</p>
<span id="index-Precision"></span>
<span id="index-mpfr_005fprec_005ft"></span>
<p>The <em>precision</em> is the number of bits used to represent the significand
of a floating-point number;
the corresponding C data type is <code>mpfr_prec_t</code>.
The precision can be any integer between <code>MPFR_PREC_MIN</code> and
<code>MPFR_PREC_MAX</code>. In the current implementation, <code>MPFR_PREC_MIN</code>
is equal to 1.
</p>
<p>Warning! MPFR needs to increase the precision internally, in order to
provide accurate results (and in particular, correct rounding). Do not
attempt to set the precision to any value near <code>MPFR_PREC_MAX</code>,
otherwise MPFR will abort due to an assertion failure. Moreover, you
may reach some memory limit on your platform, in which case the program
may abort, crash or have undefined behavior (depending on your C
implementation).
</p>
<span id="index-Exponent"></span>
<span id="index-mpfr_005fexp_005ft"></span>
<p>An <em>exponent</em> is a component of a regular floating-point number.
Its C data type is <code>mpfr_exp_t</code>. Valid exponents are restricted
to a subset of this type, and the exponent range can be changed globally
as described in <a href="Exception-Related-Functions.html#Exception-Related-Functions">Exception Related Functions</a>. Special values do not
have an exponent.
</p>
<span id="index-Rounding-Modes"></span>
<span id="index-mpfr_005frnd_005ft"></span>
<p>The <em>rounding mode</em> specifies the way to round the result of a
floating-point operation, in case the exact result can not be represented
exactly in the destination;
the corresponding C data type is <code>mpfr_rnd_t</code>.
</p>
<span id="index-Group-of-flags"></span>
<span id="index-mpfr_005fflags_005ft"></span>
<p>MPFR has a global (or per-thread) flag for each supported exception and
provides operations on flags (<a href="Exceptions.html#Exceptions">Exceptions</a>). This C data type is used
to represent a group of flags (or a mask).
</p>
<hr>
<div class="header">
<p>
Next: <a href="MPFR-Variable-Conventions.html#MPFR-Variable-Conventions" accesskey="n" rel="next">MPFR Variable Conventions</a>, Previous: <a href="Headers-and-Libraries.html#Headers-and-Libraries" accesskey="p" rel="prev">Headers and Libraries</a>, Up: <a href="MPFR-Basics.html#MPFR-Basics" accesskey="u" rel="up">MPFR Basics</a> &nbsp; [<a href="Concept-Index.html#Concept-Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
